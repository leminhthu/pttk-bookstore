package com.ptit.book.store.controller;

import com.ptit.book.store.dao.CartDao;

//import com.ptit.book.store.dao.CartItemDao;
import com.ptit.book.store.dao.ItemDao;
import com.ptit.book.store.model.Cart;
import com.ptit.book.store.model.CartItem;
import com.ptit.book.store.model.Customer;
import com.ptit.book.store.model.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/cart")
public class CartController {

    @Autowired
    private CartDao cartDao;

    @Autowired
    private ItemDao itemDao;

//    @Autowired
//    private CartItemDao cartItemDao;

    @GetMapping("/add")
    public String addItemToCart(@RequestParam("item_id") int itemId, HttpSession session, HttpServletRequest request){
        CartItem cartItem = (CartItem) session.getAttribute("cartItem");
        int amount = Integer.parseInt(request.getParameter("amount"));

        Item item = itemDao.getItemById(itemId);
        Integer existedAmount = cartDao.getExistedAmount(cartItem.getCart().getId(), item.getId());

        cartItem.setAmount(amount+existedAmount);
        cartItem.setItem(item);
        System.out.println(cartItem);

        CartItem cartItemAdded = cartDao.addItemInCart(cartItem);
        return "redirect:/home";
    }

    @GetMapping
    public String getCartForm(HttpSession session, Model model){
        Cart cart = (Cart) session.getAttribute("cart");
        Customer customer = (Customer) session.getAttribute("user");
        session.setAttribute("user", customer);

        List<CartItem> cartItemList = cartDao.getListItem(cart.getId());
        System.out.println(cartItemList);
        model.addAttribute("cartList", cartItemList);

        return "Cart";
    }
    @PostMapping
    public String getOrderItem(@RequestParam("cartItemIds") ArrayList<Integer> cartItemIds, HttpSession session, Model model) {
        System.out.println("cart postmapping");

        ArrayList<CartItem> cartItems = (ArrayList<CartItem>) cartItemIds.stream().map(id -> cartDao.getCartItemById(id))
                .collect(Collectors.toList());

        session.setAttribute("user",(Customer)session.getAttribute("user"));
        session.setAttribute("chosenCartItems", cartItems);
        return "redirect:/order";
    }

}
