package com.ptit.book.store.controller;

//import com.ptit.book.store.dao.CartItemDao;
import com.ptit.book.store.dao.OrderDao;
import com.ptit.book.store.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/order")
public class OrderController {

//    @Autowired
//    private CartItemDao cartItemDao;

    @Autowired
    private OrderDao orderDao;

    @GetMapping
    public String getOrderForm( HttpSession session, Model model){

        Customer customer = (Customer) session.getAttribute("user");
        ArrayList<CartItem> cartItems = (ArrayList<CartItem>) session.getAttribute("chosenCartItems");

        model.addAttribute("chosenCartItems", cartItems);
//        System.out.println("getmapping");
//        cartItems.stream().forEach(item ->System.out.println(item.getId()));
        model.addAttribute("totalPayment", calculatePayment(cartItems));
        model.addAttribute("user", customer);

        session.setAttribute("user",customer);
        session.setAttribute("orderItems",cartItems);

        return "Order";
    }

//    @PostMapping
//    public String getOrderItem(@RequestParam("cartItemIds") ArrayList<Integer> cartItemIds, HttpSession session, Model model) {
//        System.out.println("postmapping");
//
//        ArrayList<CartItem> cartItems = (ArrayList<CartItem>) cartItemIds.stream().map(id -> cartItemDao.getCartItemById(id))
//                                        .collect(Collectors.toList());
//
//        session.setAttribute("user",(Customer)session.getAttribute("user"));
//        session.setAttribute("chosenCartItems", cartItems);
//
//
////        Customer customer = (Customer) session.getAttribute("user");
////
////        Order order = Order.builder().customer(customer)
////                .creationDate(Date.valueOf(LocalDate.now()))
//////                .description(desc)
////                .build();
////
////        Order addedOrder = orderDao.addOrder(order);
////        ArrayList<OrderItem> orderItems = (ArrayList<OrderItem>) cartItems.stream()
////                                            .map(cartItem->OrderItem.builder().cartItem(cartItem).order(addedOrder).build())
////                                            .collect(Collectors.toList());
////        orderDao.addOrderItems(orderItems);
//       return "Order";
//    }

    @PostMapping("/add")
    public String addOrder(HttpSession session) {

        System.out.println("postmapping2");
        Customer customer = (Customer) session.getAttribute("user");
        ArrayList<CartItem> cartItems = (ArrayList<CartItem>) session.getAttribute("chosenCartItems") ;

        Order order = Order.builder().customer(customer)
                .creationDate(Date.valueOf(LocalDate.now()))
//                .description(desc)
                .build();

        Order addedOrder = orderDao.addOrder(order);
        ArrayList<OrderItem> orderItems = (ArrayList<OrderItem>) cartItems.stream()
                                            .map(cartItem->OrderItem.builder().cartItem(cartItem).order(addedOrder).build())
                                            .collect(Collectors.toList());
        orderDao.addOrderItems(orderItems);
        return "redirect:/home";

    }

    private long calculatePayment(ArrayList<CartItem> cartItems) {
        return cartItems.stream().mapToLong(cartItem -> (long) (cartItem.getAmount() * cartItem.getItem().getPrice())).sum();

    }

}
