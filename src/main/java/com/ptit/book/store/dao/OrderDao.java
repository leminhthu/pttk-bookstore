package com.ptit.book.store.dao;

import com.ptit.book.store.model.Order;
import com.ptit.book.store.model.OrderItem;

import java.util.ArrayList;
import java.util.List;

public interface OrderDao {

    public Order addOrder(Order order);

    public ArrayList<OrderItem> addOrderItems(ArrayList<OrderItem> orderItems);
}
