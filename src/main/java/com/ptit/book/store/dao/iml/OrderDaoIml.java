package com.ptit.book.store.dao.iml;

import com.ptit.book.store.dao.OrderDao;
import com.ptit.book.store.model.Order;
import com.ptit.book.store.model.OrderItem;
import com.ptit.book.store.repository.CartItemRepository;
import com.ptit.book.store.repository.OrderItemRepository;
import com.ptit.book.store.repository.OrderRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.stream.Collectors;

@Service
public class OrderDaoIml implements OrderDao {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private OrderItemRepository orderItemRepository;

    @Override
    public Order addOrder(Order order) {
        Order savedOrder = orderRepository.save(order);
        return savedOrder;
    }

    @Override
    public ArrayList<OrderItem> addOrderItems(ArrayList<OrderItem> orderItems) {
        return (ArrayList<OrderItem>) orderItems.stream().
                map(orderItem -> orderItemRepository.save(orderItem))
                .collect(Collectors.toList());
    }
}
